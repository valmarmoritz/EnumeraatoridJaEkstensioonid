﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    static class E
    {
        public static string Trüki(this String x)
        {
            Console.WriteLine($"{x} - kell {DateTime.Now.ToLongTimeString()}");
            return x;
        }

        public static IEnumerable<int> Paaris (this IEnumerable<int> mass)
        {
            foreach(var x in mass) if (x % 2 == 0) yield return x;
        }

        public static IEnumerable<int> Suured (this IEnumerable<int> mass)
        {
            foreach (var x in mass) if (x > 5) yield return x;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            "ALGUS".Trüki();
            E.Trüki("alustasin");
            // System.Threading.Thread.Sleep(4000);

            int[] arvud = {0,1,2,3,4,5,6,7,8,9,10};
            foreach (var x in arvud
                .Paaris()                       // siit saab ridahaaval välja kommenteerida neid 'filtreid', mida me ei taha hetkel läbida
                .Suured()
                ) Console.WriteLine(x);

            E.Trüki("lõpetasin");
            "LÕPP".Trüki();
        }
    }
}
